<?php
   /*
   Plugin Name: Post Popularity - Richie Santoyo
   Plugin URI: http://ranchitoplay.com
   Description: Da seguimiento a la popularidad de los posts y despliega una lista con los posts más populares.
   Version: 1.1
   Author: Ricardo Santoyo Reyes
   Author URI: https://www.linkedin.com/in/ricardo-santoyo-reyes-413715142/
   License: MIT
   */

   /**
   * Agrega una vista a los posts visitados
   *
   * Encuentra las vistas actuales de un posts y le agrega una más actualizando
   * el postmeta. El meta key usado es "richpop_views".
   *
   * @global object $post El objeto del post
   * @return integer $new_views El número de vistas que tiene el post
   *
   */
   function richpop_add_view(){
     if(is_single()){
       global $post;
       $current_views = get_post_meta($post->ID, "richpop_views", true);
       if(!isset($current_views) OR empty($current_views) OR !is_numeric($current_views)){
         $current_views = 0;
       }
       $new_views = $current_views + 1;
       update_post_meta($post->ID, "richpop_views", $new_views);
       return $new_views;
     }
   }
   add_action("wp_head", "richpop_add_view");

   /**
   * Regresa el número de vistas que tiene un post
   *
   * Encentra el número de vistas que tiene un post, regresando 0 si no tiene ninguna
   *
   * @global object $post El objeto del post
   * @return integer $current_views El número de vistas que tiene el post
   *
   */
  function richpop_get_view_count() {
     global $post;
     $current_views = get_post_meta($post->ID, "richpop_views", true);
     if(!isset($current_views) OR empty($current_views) OR !is_numeric($current_views) ) {
        $current_views = 0;
     }

     return $current_views;
  }

  /**
   * Muestra el número de vistas de un post
   *
   * Encuentra el número actual de vistas de un post y lo muestra junto con texto opcional
   *
   * @global object $post El objeto del post
   * @uses richpop_get_view_count()
   *
   * @param string $singular El término singular del texto
   * @param string $plural El término plural del texto
   * @param string $before Texto antes del contador
   *
   * @return string $views_text Las vistas a mostrar
   *
   */
  function richpop_show_views($singular = "vista", $plural = "vistas", $before = "Este post tiene: ") {
     global $post;
     $current_views = richpop_get_view_count();

     $views_text = $before . $current_views . " ";

     if ($current_views == 1) {
        $views_text .= $singular;
     }
     else {
        $views_text .= $plural;
     }

     echo $views_text;

  }

  /**
   * Muestra una lista de posts ordenados por popularidad
   *
   * Muestra una lista simple de títulos ordenados por su total de vistas
   *
   * @param integer $post_count El número de posts a mostrar
   *
   */
   function richpop_popularity_list($post_count = 10) {
    $args = array(
      "posts_per_page" => $post_count,
      "post_type" => "post",
      "post_status" => "publish",
      "meta_key" => "richpop_views",
      "orderby" => "meta_value_num",
      "order" => "DESC"
    );

    $richpop_list = new WP_Query($args);

    if($richpop_list->have_posts()) { echo "<ul>"; }

    while ( $richpop_list->have_posts() ) : $richpop_list->the_post();
      echo '<li><a href="'.get_permalink($post->ID).'">'.the_title('', '', false).'</a></li>';
    endwhile;

    if($richpop_list->have_posts()) { echo "</ul>";}
   }

   /**
    * Regresa un array con los ID de posts ordenados por sus vistas
    *
    * Muestra una los ID de posts ordenados por sus vistas
    *
    * @param integer $post_count El número de posts a mostrar
    *
    */
    function richpop_popularity_raw($post_count = 10) {
     $args = array(
       "posts_per_page" => $post_count,
       "post_type" => "post",
       "post_status" => "publish",
       "meta_key" => "richpop_views",
       "orderby" => "meta_value_num",
       "order" => "DESC"
     );

     $richpop_list = new WP_Query($args);

     while ( $richpop_list->have_posts() ) : $richpop_list->the_post();
       $post_array[] = get_the_ID();
     endwhile;

     return $post_array;
    }

    /**
     * Shortcode para utilizar las funciones
     *
     * Regresa una lista de posts populares mediante el shortcode [richpop]
     *
     * @param array $atts Atributos del shortcode
     *
     */
    function richpop_func( $atts ){
      $a = shortcode_atts( array(
        'count' => 10,
        'list' => 'si',
        'videopro' => 'no',
        'order' => 'DESC'
      ), $atts );

      if($a['videopro'] === 'no'){
        if($a['list'] === 'no'){
          $post_list = richpop_popularity_raw($a['count']);

          if($a['order'] === 'ASC'){
            for ($i = (count($post_list) - 1); $i >= 0; $i--) {
              $aux_array[] = $post_list[$i];
            }
            $post_list = $aux_array;
          }

          $post_list = implode(',', $post_list);

          echo $post_list;
        } else if($a['list'] === 'si'){
          richpop_popularity_list($a['count']);
        }
      } else if($a['videopro'] === 'si') {
        $post_list = richpop_popularity_raw($a['count']);

        if($a['order'] === 'ASC'){
          for ($i = (count($post_list) - 1); $i >= 0; $i--) {
            $aux_array[] = $post_list[$i];
          }
          $post_list = $aux_array;
        }

        $post_list = implode(',', $post_list);

        $videopro_shortcode = '[videopro_slider title="Lo más visto" count="' . $a['count'] . '" layout="2" condition="input" ids="' . $post_list . '" show_datetime="0" show_author="0" show_comment_count="0" show_like="0" show_duration="0"]';

        echo do_shortcode($videopro_shortcode);
      }
    }
    add_shortcode( 'richpop', 'richpop_func' );
?>

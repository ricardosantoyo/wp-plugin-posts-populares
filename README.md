# Plugin Posts Populares #

Muestra una lista con los posts más populares del blog. Compatible con VideoPro para mostrar un slider de entradas

### ¿Para qué es? ###

* Muestra una lista de los posts más populares 
* Crea un slider de VideoPro (Se tiene que tener instalado el tema)
* Devuelve los IDs de los posts más populares

### Cómo instalarlo ###

* Descargar richieplugin.zip
* Desde el administrador de WordPress, ir a la sección Plugins
* Dar click en Añadir Plugin
* Dar click en Subir Plugin
* Seleccionar el archivo descargado y dar en aceptar
* Activarlo

### Cómo usarlo ###

* Usar el shortcode [richpop] con sus respectivos argumentos

### Argumentos = default: ###

* count = 10: Número de posts a mostrar
* list = si/no: Mostrar el contenido como lista con hipervínculos (no: devuelve un string de los IDs separados por comas)
* videopro = si/no: Mostrar los posts como slider de VideoPro
* order = ASC/DESC: Orden en que se mostrarán (sólo con videopro = si)

### Créditos ###
* Ing. Ricardo Santoyo Reyes, Web Developer at Telemétrika S.A. de C.V.
* Telemétrica S.A. de C.V.

### Disclaimer ###
* No tengo relación alguna con VideoPro ni con Theme Forest, el contenido de este proyecto es meramente educativo.
* Pueden utilizar el código de este proyecto para uso propio, modificándolo si es necesario. Sólo pido se conserven los datos de contacto y se den créditos.